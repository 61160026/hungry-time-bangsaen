import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hungry_time_bangsaen/screens/editPage.dart';

class selectEditList extends StatefulWidget {
  @override
  _selectEditListState createState() => _selectEditListState();
}

class _selectEditListState extends State<selectEditList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: selectEdit(),
          )
        ],
      ),
    );
  }
}

class selectEdit extends StatefulWidget {
  selectEdit({Key? key}) : super(key: key);

  @override
  _selectEditState createState() => _selectEditState();
}

class _selectEditState extends State<selectEdit> {
  final Stream<QuerySnapshot> _storeStream =
      FirebaseFirestore.instance.collection('users').snapshots();

  CollectionReference users = FirebaseFirestore.instance.collection('users');

  Future<void> delUser(userId) {
    return users
        .doc(userId)
        .delete()
        .then((value) => print('User Delete'))
        .catchError((error) => print('Failed to delete user: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Edit Account',
          style: GoogleFonts.anton(),
        ),
        backgroundColor: Colors.lightGreen,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: _storeStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return ListView(
              children: snapshot.data!.docs.map((doc) {
                return Card(
                  child: new InkWell(
                    child: Container(
                      height: 80,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        children: [
                          Container(
                            padding: EdgeInsets.only(left: 10, right: 10),
                            width: 70.0,
                            height: 70.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/user%20(1).png?alt=media&token=7a5b859d-8575-4206-8ac0-dd59a955b899'))),
                          ),
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(
                                  top: 10, left: 10, right: 10, bottom: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        doc['name_company'],
                                        style: GoogleFonts.athiti(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                          height: 2,
                                        ),
                                      ),
                                      Row(
                                        children: [
                                          IconButton(
                                              onPressed: () {
                                                Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                        builder: (context) =>
                                                            EditPage(userId: doc.id)));
                                              },
                                              icon: Icon(Icons.edit)),
                                          IconButton(
                                              onPressed: () async {
                                                await delUser(doc.id);
                                              },
                                              icon: Icon(Icons.delete)),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
        },
      ),
    );
  }
}
