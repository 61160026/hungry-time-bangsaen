import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class showListmenus extends StatefulWidget {
  @override
  _showListmenusState createState() => _showListmenusState();
}

class _showListmenusState extends State<showListmenus> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: ListMenus(),
          )
        ],
      ),
    );
  }
}

class ListMenus extends StatefulWidget {
  ListMenus({Key? key}) : super(key: key);

  @override
  _ListMenusState createState() => _ListMenusState();
}

class _ListMenusState extends State<ListMenus> {
  final Stream<QuerySnapshot> _listMenuStream = FirebaseFirestore.instance
      .collection('menus')
      .snapshots();

  CollectionReference listMenus = FirebaseFirestore.instance.collection('menus');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream: _listMenuStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return ListView(
              children: snapshot.data!.docs.map((doc) {
                return Card(
                  child: new InkWell(
                    child: Container(
                      height: 80,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        children: [
                          Container(
                            width: 80.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/p3.png?alt=media&token=130eee1d-6f3f-44e2-b0bd-652c8cebb9e8'))),
                          ),
                          Expanded(
                            child: Container(
                              padding:
                                  EdgeInsets.only(top: 5, left: 15, right: 10, bottom: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        doc['name'],
                                        style: GoogleFonts.athiti(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold,
                                          height: 2,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    doc['price'].toString()+" บาท",
                                    style: GoogleFonts.prompt(
                                        fontSize: 15, height: 1.5),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
        },
      ),
    );
  }
}
