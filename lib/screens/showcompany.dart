class TopFood {
  const TopFood({
    required this.image,
    required this.name,
    required this.desc,
    required this.rating,
  });

  final String image;
  final String name;
  final String desc;
  final String rating;

  static List<TopFood> getPopularAllRestaurants() {
    return [
      TopFood(
        image: 'assets/food1.png',
        name: 'Veg King',
        desc: 'Thai Food',
        rating: '4.1',
      ),
      TopFood(
        image: 'assets/food2.png',
        name: 'Adyar Hotel',
        desc: 'South Indian',
        rating: '4.3',
      ),
      TopFood(
        image: 'assets/food3.png',
        name: 'Chennai Mirchi',
        desc: 'South Indian',
        rating: '4.5',
      ),
      TopFood(
        image: 'assets/food4.png',
        name: 'BBQ Nation',
        desc: 'South Indian',
        rating: '4.3',
      ),
      TopFood(
        image: 'assets/food5.png',
        name: 'A2B Chennai',
        desc: 'South Indian',
        rating: '4.6',
      ),
      TopFood(
        image: 'assets/food6.png',
        name: 'Dinner Expresss',
        desc: 'North Indian',
        rating: '4.8',
      ),
    ];
  }
}
