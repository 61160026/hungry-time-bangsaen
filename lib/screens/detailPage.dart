import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:hungry_time_bangsaen/screens/listMenu.dart';

class detailPage extends StatefulWidget {
  final String title;
  detailPage({Key? key, required this.title}) : super(key: key);

  @override
  _detailPageState createState() => _detailPageState(this.title);
}

class _detailPageState extends State<detailPage> {
  String title;

  final Stream<QuerySnapshot> _companyStream =
      FirebaseFirestore.instance.collection('users').snapshots();

  CollectionReference companys = FirebaseFirestore.instance.collection('users');
  _detailPageState(this.title);

  Completer<GoogleMapController> _controller = Completer();
  late GoogleMapController controller;
  List<Marker> markers = [
    Marker(
        markerId: MarkerId('value'),
        position: LatLng(13.283930275316568, 100.9289412749853))
  ];

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(13.283930275316568, 100.9289412749853),
    zoom: 14.4746,
  );

  static final CameraPosition _kLake = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(13.283930275316568, 100.9289412749853),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  @override
  void initState() {
    super.initState();
    setState(() {
      this.title = title;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            title,
            style: GoogleFonts.anton(),
          ),
          backgroundColor: Colors.lightGreen,
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection('users')
              .where('name_company', isEqualTo: title)
              .snapshots(),
          builder: (context, snapshot) {
            return ListView(
              children: snapshot.data!.docs.map((doc) {
                return Container(
                  padding: EdgeInsets.all(15),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            doc['name_company'],
                            style: GoogleFonts.athiti(
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                              height: 1.5,
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.phone,
                            size: 20,
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Text(
                            'เบอร์โทรศัพท์ : ' + doc['tel'],
                            style:
                                GoogleFonts.prompt(fontSize: 18, height: 1.5),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 6,
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.star,
                            size: 20,
                            color: Colors.orangeAccent,
                          ),
                          SizedBox(
                            width: 4,
                          ),
                          Text(
                            'rating : ' + doc['rating'],
                            style:
                                GoogleFonts.prompt(fontSize: 18, height: 1.5),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Container(
                        height: 250,
                        child: GoogleMap(
                          markers: markers.toSet(),
                          mapType: MapType.normal,
                          initialCameraPosition: _kGooglePlex,
                          onMapCreated: (GoogleMapController controller) {
                            _controller.complete(controller);
                          },
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(child: showListmenus())
                    ],
                  ),
                );
              }).toList(),
            );
          },
        ));
  }
}
