import 'package:flutter/material.dart';
import 'package:hungry_time_bangsaen/screens/companyList.dart';
import 'package:hungry_time_bangsaen/screens/login.dart';
import 'package:hungry_time_bangsaen/screens/register.dart';
import 'package:hungry_time_bangsaen/screens/searchMenu.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hungry_time_bangsaen/screens/selectEdit.dart';

class TestList {
  final String title;
  final String description;

  const TestList(this.title, this.description);
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Welcome to Hungry Time Bangsaen',
            style: GoogleFonts.anton(),
          ),
          backgroundColor: Colors.lightGreen,
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              DrawerHeader(
                child: Column(
                  children: <Widget>[
                    Container(
                      width: 100,
                      height: 100,
                      alignment: Alignment.center,
                      child: Image.asset(
                        "assets/take-away.png",
                      ),
                    ),
                    Text('Hungry Time Bangsaen',
                        style: TextStyle(fontSize: 20.0, fontFamily: 'Anton')),
                  ],
                ),
                decoration: BoxDecoration(color: Colors.lightGreen),
              ),
              ListTile(
                title: Text('Login for Business', style: GoogleFonts.anton(fontSize: 20.0)),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => LoginPage()));
                },
              ),
              ListTile(
                title: Text(
                  'Register for Business',
                  style: GoogleFonts.anton(fontSize: 20.0),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SignUpPage()));
                },
              ),
              ListTile(
                title: Text(
                  'Edit Account',
                  style: GoogleFonts.anton(fontSize: 20.0),
                ),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => selectEdit()));
                },
              ),
            ],
          ),
        ),
        body: Container(
          child: Column(
            children: [
              SearchWidget(),
              CompanyNameList(),
            ],
          ),
        ));
  }
}
