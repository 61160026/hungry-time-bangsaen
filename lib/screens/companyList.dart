import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:hungry_time_bangsaen/screens/detailPage.dart';

class CompanyNameList extends StatefulWidget {
  @override
  _CompanyNameListState createState() => _CompanyNameListState();
}

class _CompanyNameListState extends State<CompanyNameList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 600,
      width: double.infinity,
      child: Column(
        children: <Widget>[
          Expanded(
            child: CompanyList(),
          )
        ],
      ),
    );
  }
}

class CompanyList extends StatefulWidget {
  CompanyList({Key? key}) : super(key: key);

  @override
  _CompanyListState createState() => _CompanyListState();
}

class _CompanyListState extends State<CompanyList> {
  final Stream<QuerySnapshot> _storeStream =
      FirebaseFirestore.instance.collection('users').snapshots();

  CollectionReference stores = FirebaseFirestore.instance.collection('users');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder<QuerySnapshot>(
        stream: _storeStream,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else
            return ListView(
              children: snapshot.data!.docs.map((doc) {
                return Card(
                  child: new InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  detailPage(title: doc['name_company'])));
                    },
                    child: Container(
                      height: 100,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Row(
                        children: [
                          Container(
                            width: 80.0,
                            height: 80.0,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: NetworkImage(
                                        'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/fast-food.png?alt=media&token=0a470ccd-5b49-45ff-a66c-415cff49106b'))),
                          ),
                          Expanded(
                            child: Container(
                              padding:
                                  EdgeInsets.only(top: 10, left: 10, right: 10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        doc['name_company'],
                                        style: GoogleFonts.athiti(
                                          fontSize: 25,
                                          fontWeight: FontWeight.bold,
                                          height: 2,
                                        ),
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios_outlined,
                                        size: 15,
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Icon(
                                        Icons.star,
                                        size: 20,
                                        color: Colors.orangeAccent,
                                      ),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      Text(
                                        'rating : ' + doc['rating'],
                                        style: GoogleFonts.prompt(
                                            fontSize: 15, height: 1.5),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                );
              }).toList(),
            );
        },
      ),
    );
  }
}
